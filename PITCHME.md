# REDCap on a budget at ACU

Aidan Wilson

aidan.wilson@acu.edu.au

Note:

I'm the Intersect eResearch Analyst for Australian Catholic Universtiy, where managing and supporting the REDCap instance is probably the majority of my role.

---

## Efficiencies for small teams

- Authentication with AAF 
- Integration with HREC

Note:

At ACU, our resources are fairly limited. While we only have a couple of hundred users, and maybe only a few dozen active users at any one time, managing the system, providing training, user support and consulting, even assisting with building projects, consumes a lot of time. 

Naturally we try to find efficiencies wherever possible. Using AAF for authentication was one such efficiency that we've been able to take advantage of, as it takes care of a lot of the manual effort in creating and managing user accounts.

Integrations with HREC for us is still a manual task, but I will discuss the business processes we have in place to facilitate researchers using REDCap with as little friction as possible.

---

## Authentication with AAF

- Set up with table-based account only
- Grew to be too complex
	- Sponsored external accounts
	- Staff turnover
- No experience in LDAP/Shibboleth authentication
- AAF Rapidconnect added to REDCap core ~2017
- AAF for all internal and any AAF collaborators
- Table-based for everyone else (with expiry)

Note:

REDCap was set up at ACU in 2015 using table-based accounts as there was little resources either in the Research office or in IT to configure LDAP or Shibboleth. As such, managing user accounts became one of the day-to-day tasks for the eResearch team. We had a policy whereby any staff or research student could get their own account simply by asking, but non-research students or anyone external would need to have an account sponsored by someone else. This worked fine, but after a couple of years keeping on top of accounts became a bit too much to handle, especially with the number of sponsored accounts.

By then, Nicholas Miller from the University of Sydney and Ryan Brown from UniSA, had written some code for AAF Rapidconnect, which takes a lot of the complexity out of using AAF, and submitted it to REDCap's core. I managed to get it set up on our dev instance and after testing for a couple of weeks, we enabled it on production. We found the process pretty straightforward although I hit a couple of snags along the way, which I documented in an article on the REDCap community site for others.

+++

![AAF_REDCap](assets/aaf_redcap.png)

Note:

By then, Nicholas Miller from the University of Sydney and Ryan Brown from UniSA, had written some code for AAF Rapidconnect, which takes a lot of the complexity out of using AAF, and submitted it to REDCap's core. I managed to get it set up on our dev instance and after testing for a couple of weeks, we enabled it on production. We found the process pretty straightforward although I hit a couple of snags along the way, which I documented in an article on the REDCap community site for others.

With the AAF settings in REDCap, we can enter an attribute that will identify someone as being local, for us that attribute is member@acu.edu.au, and we can automatically configure it such that locals automatically get the right to create projects. With this one setting, we can seamlessly and automatically allow internal ACU people to create projects, but still allow non-ACU identities to authenticate, say researcher from other universities who are collaborating with our researchers. With AAF they can authenticate, and an internal ACU person can add them to their project with appropriate permissions.

We still have table-based accounts for external collaborators who cannot authenticate with AAF, as there are plenty of people in that category, particularly in the health and medical projects that REDCap is predominantly used for, as well as any international collaborators.

+++

![Rapidconnect](assets/aaf_rapidconnect.png)

Note:

By then, Nicholas Miller from the University of Sydney and Ryan Brown from UniSA, had written some code for AAF Rapidconnect, which takes a lot of the complexity out of using AAF, and submitted it to REDCap's core. I managed to get it set up on our dev instance and after testing for a couple of weeks, we enabled it on production. We found the process pretty straightforward although I hit a couple of snags along the way, which I documented in an article on the REDCap community site for others.

With the AAF settings in REDCap, we can enter an attribute that will identify someone as being local, for us that attribute is member@acu.edu.au, and we can automatically configure it such that locals automatically get the right to create projects. With this one setting, we can seamlessly and automatically allow internal ACU people to create projects, but still allow non-ACU identities to authenticate, say researcher from other universities who are collaborating with our researchers. With AAF they can authenticate, and an internal ACU person can add them to their project with appropriate permissions.

We still have table-based accounts for external collaborators who cannot authenticate with AAF, as there are plenty of people in that category, particularly in the health and medical projects that REDCap is predominantly used for, as well as any international collaborators.

---

## Integration with HREC

- No knowledge of REDCap among HREC or Ethics team
- Awareness raising:
	- Presentations to HREC on REDCap vs. Qualtrics
	- Educating Ethics team on capabilities/limitations
- Ethics approval required before project moved to production
- eConsent approved without Ethics modification

Note:

When REDCap was installed at ACU, the Ethics committee knew very little about it, which is problematic given the fact that ethics often advise researchers on which tools they should be using for protecting the privacy of their research participants. As such, I have put a fair bit of effort into keeping the Ethics committee and the ethics team in the research office, up to date on what REDCap is, what it can do, what its limitations are, and some of its features for protecting privacy.

The result of this is that the ethics team are better equipped to recommend to researchers who are working with potentially risky or sensitive data, that they should use REDCap for collecting and managing it, and that they can speak to us in eResearch for support.

At the other end of the process, when researchers begin collecting data, we have built a standard operating procedure whereby a researcher requests their project to move to production, which triggers an alert to the administrators. We then retrieve the project's HREC number from the main project settings, or first request the HREC number from the team if it is missing, and email the ethics team with their number, to check on the status of their application. We only approve production projects that have ethics approval.

That's about the extent of this integration. We would like the Ethics team to interact more with REDCap but it's still outside their comfort zone at the moment.

One important point though is that COVID-19 saw lots of projects halted due to social distancing. REDCap, and the eConsent framework in particular was central to allowing research projects to continue, and the Ethics team, once they saw the eConsent framework in action, were happy to approve any project modifications to use it.

---

## Summary

- REDCap is manageable for a small team with limited technical skills
- AAF Rapidconnect helps significantly
- Interest from other stakeholders, HREC, RO, critical

This slidedeck: [inter.fyi/redcapbof](https://inter.fyi/redcapbof)

Note:

Our experience at ACU is that REDCap is very easy for a small team to manage and administer, and to do so effectively. The task is made much easier with tools like AAF and Rapidconnect, and having other areas of the institution, such as ethics, have some level of knowledge about REDCap and where it is useful, can really help. And it brings enormous benefits in allowing researchers to run projects from as simple as an anonymous survey to complex, randomised clinical trials, in a very quick and agile way.
